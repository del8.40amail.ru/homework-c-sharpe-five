﻿using System;
using System.Linq;

namespace HomeWorkNumberFive
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите текст через пробелы для разделения его по словам.");
            PrintSplitText(SplitText());
            Console.WriteLine();
            Console.WriteLine("Введите текст через пробелы для переворачивания его задом наперед.");
            PrintReverseText(ReverseText());
            Console.ReadKey();
        }
        static string[] SplitText()
        {
            string data = Console.ReadLine();
            string[] splitData = data.Split(" ");
            return splitData;
        }
        static string[] ReverseText()
        {
            string[] splitDataText = SplitText();
            Array.Reverse(splitDataText);
            return splitDataText;
        }
        static void PrintSplitText(string[] text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Console.WriteLine(text[i]);
            }
        }
        static void PrintReverseText(string[] text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Console.Write($"{text[i]} ");
            }
        }
    }
}
